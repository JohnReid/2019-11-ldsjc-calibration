\documentclass[xcolor={usenames, dvipsnames, svgnames, table}]{beamer}

%
% Customise beamer
\usetheme{metropolis}
\usecolortheme[snowy]{owl}  % Dark on light
\setbeamertemplate{footline}{}  % Completely remove footline
\setbeamertemplate{frametitle continuation}{[\insertcontinuationcount]}  % Nicer numbering
%
% Uncomment line below to make printable notes
% \setbeameroption{show only notes}
%
% Remove icons from bibliography
% https://tex.stackexchange.com/a/53131/39498
\setbeamertemplate{bibliography item}{}


%
% PDF metadata
\usepackage{hyperref}
\hypersetup{%
  pdfauthor={John Reid},
  % pdftitle={<TITLE>},
  % pdfkeywords={<KEYWORDS>},
  pdfcreator={xelatex},
  % pdfproducer={xelatex},
}


%
% Math
\usepackage{unicode-math}
\usepackage{amsmath}  % For general maths
% \usepackage{pifont}  % http://ctan.org/pkg/pifont
% \usepackage{bm}  % For bold math
\usepackage{esdiff}  % For derivatives
\usepackage{mathtools}
% \usepackage{siunitx}
% \usepackage{newtxmath}
%\usepackage{amsthm}
%\usepackage{thmtools}
%\usepackage{wasysym}
%\usepackage{amssymb} % not with newtxmath
%\usepackage{mathabx} % not with newtxmath
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}


%
% Miscellaneous
%\usepackage{hanging}  % Hanging paragraphs
\usepackage{booktabs}  % Nice tables
\usepackage[normalem]{ulem} % Underlining
% \usepackage{minted}  % For syntax highlighting source code
\usepackage{setspace}  % For setstretch
\usepackage{calc}      % For widthof()
% \usepackage{fontawesome}  % For Twitter symbol, etc...
\usepackage{silence}   % To silence some warning
% \WarningFilter{biblatex}{Patching footnotes failed}


%
% TikZ
\usepackage{tikz}
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background, main, foreground}
\usetikzlibrary{%
  arrows,
  arrows.meta,
  shapes,
  decorations.pathmorphing,
  backgrounds,
  positioning,
  fit,
  shadows,
  calc,
  graphs,
  graphs.standard,
  bayesnet}


%
% Set up biblatex
\usepackage[
  doi=false,
  isbn=false,
  url=false,
  sorting=none,
  style=authoryear,
  backend=biber]{biblatex}
%
% We specify the database.
\addbibresource{2019-11-Calibration.bib}  % chktex 8
\DeclareCiteCommand{\citejournal}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \usebibmacro{cite}
   \usebibmacro{journal}}
  {\multicitedelim}
  {\usebibmacro{postnote}}
%
% To cite in frame footnote
\newcommand{\myfootcite}[1]{\footnote[frame]{\cite{#1}}}
%
% A footnote without a marker from
% https://tex.stackexchange.com/questions/30720/footnote-without-a-marker/30726#30726
\newcommand\blfootnote[1]{
  \begingroup
    \renewcommand\thefootnote{}\footnote[frame]{#1}
    \addtocounter{footnote}{-1}
  \endgroup
}


%
% Fonts
%
% \usepackage{fix-cm}  % just to avoid some spurious messages
% \usefonttheme{professionalfonts}  % using non standard fonts for beamer
% \usefonttheme{serif}  % default family is serif
% \setmainfont{TeX Gyre Bonum}
% \setmathfont{TeX Gyre Bonum Math}
\setmainfont{Latin Modern Roman}
\setmathfont{Latin Modern Math}


%
% Quotations
% See: https://tex.stackexchange.com/questions/365231/enclose-a-custom-quote-environment-in-quotes-from-csquotes
%
\def\signed#1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip1em
  \hbox{}\nobreak\hfill #1%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

  \newsavebox\mybox{}
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}\openautoquote\hspace*{-.7ex}}
  {\unskip\closeautoquote\vspace*{1mm}\signed{\usebox\mybox}\end{quote}}


%
% COMMANDS
%
% Todo
\newcommand{\todo}[1]{\textbf{TODO}: #1}
\newcommand{\hide}[1]{}
%
% Cpp command
\newcommand{\cpp}{C\nolinebreak\hspace{-.05em}\raisebox{.4ex}{\tiny\bf +}\nolinebreak\hspace{-.10em}\raisebox{.4ex}{\tiny\bf +}}
%
% Tick and cross
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}


%
% Document metadata
\title{Calibration of neural networks}
% \subtitle{<SUBTITLE>}
% \date{February 19, 2018}
\author{John Reid}
\institute{Blue Prism AI Labs}


%
% Document
\begin{document}

\maketitle

\begin{frame}{Calibration}
  Can NN outputs be used as probabilities?
  \begin{center}
    \textbf{Yes!}
  \end{center}

  Are they any good?
  \begin{center}
    \textbf{Possibly if calibrated}
  \end{center}
\end{frame}


\begin{frame}{Why calibrate?}
  \begin{itemize}
    \item Accuracy dominant target in ML
    \item Risky decisions made based on classifiers' predictions
    \begin{itemize}
      \item medical diagnoses
      \item self-driving cars
    \end{itemize}
    \item Ensembling
  \end{itemize}
  \begin{center}
    \begin{tabular}{cccc}
      \toprule
      \multicolumn{2}{c}{data} & \multicolumn{2}{c}{predictions} \\
      \cmidrule(lr){1-2}
      \cmidrule(lr){3-4}
      sample & label & $p$ & $q$ \\
      \midrule
      1 & 1 & 0.9 & 0.9999 \\
      2 & 0 & 0.8 & 0.9998 \\
      3 & 1 & 0.7 & 0.9997 \\
      4 & 1 & 0.6 & 0.9996 \\
      5 & 0 & 0.5 & 0.9995 \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}


\begin{frame}{What is calibration?}
  \begin{center}
    statistical consistency between \\ \emph{probabilistic predictions} and \emph{observations}
    \[
      P[Y = k\ |\ \hat{\symbf{p}}(X) = \symbf{q}] = q_k \qquad \forall \symbf{q}, k
    \]
    where $X, Y$ are random variables representing the data and its class; and $\hat{\symbf{p}}(X)$
    is the output of the classifier
  \end{center}
  \blfootnote{\cite{KulltemperaturescalingObtaining2019}}
\end{frame}


\begin{frame}{Expected calibration error}
  \[
    \textnormal{classwise-ECE}
      = \frac{1}{K} \sum_{k=1}^K \sum_{m=1}^M
      \frac{|B_{m, k}|}{N} |y_k(B_{m, k}) - \hat{p}_k(B_{m, k})|
  \]
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/calibration}
  \end{center}
\end{frame}


\begin{frame}{Modern methods miscalibrate}
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/modern-miscalibration}
  \end{center}
  \blfootnote{\cite{GuoCalibrationModernNeural2017}}
\end{frame}


\begin{frame}{Temperature scaling}
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/temp-scaling}
  \end{center}
\end{frame}


\begin{frame}{NLL over-fitting}
  \begin{center}
    \includegraphics[height=.8\textheight]{Figures/NLL-overfitting}
  \end{center}
\end{frame}


\begin{frame}{Beyond temperature scaling}
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/beyond-temp-scaling}
  \end{center}
\end{frame}


\begin{frame}{Scoring rules}
  Summary statistic for evaluation of probabilistic prediction, $P$, in light of observation, $y$.

  \begin{align*}
    S(P, y) &\in \mathbb{R} \\
    S(P, Q) &= \mathbb{E}_{y \sim Q}[S(P, y)]
  \end{align*}

  \begin{block}{Proper scoring rule:}
    \[S(Q, Q) \ge S(P, Q) \qquad \forall P, Q\]
  \end{block}

  \begin{center}
    \emph{Your loss is a proper scoring rule}
  \end{center}
\end{frame}


\begin{frame}{Scoring rule geometry}
  \begin{center}
    \includegraphics[width=.9\textwidth]{Figures/scores-geometry}
  \end{center}
  \blfootnote{\cite{GneitingStrictlyProperScoring2007}}
\end{frame}


\begin{frame}{Scoring functions}
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/scoring-functions}
  \end{center}
\end{frame}


% \section{References}
\begin{frame}[allowframebreaks]{References}
  % \renewcommand*{\bibfont}{\scriptsize}
  \renewcommand*{\bibfont}{\footnotesize}
  \printbibliography[heading=none]{}
\end{frame}

\end{document}
